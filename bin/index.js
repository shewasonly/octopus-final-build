var chokidar = require('chokidar');

var http = require('http');

var watcher = chokidar.watch('data/alive', {ignored: /^\./, persistent: true});

watcher
	.on('add', function(path) {
		console.log('File', path, 'has been added');
		http.get("http://dev.shewasonly.co.uk/octopus_api/stillAlive.php", function(res) {
			console.log("Got response: " + res.statusCode);
		}).on('error', function(e) {
			console.log("Got error: " + e.message);
		});
	})
	.on('change', function(path) {})
	.on('unlink', function(path) {console.log('File', path, 'has been removed');})
	.on('error', function(error) {})