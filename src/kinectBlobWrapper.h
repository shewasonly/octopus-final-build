//
//  kinectBlobWrapper.h
//  kinectWrapper
//
//  Created by Jonathan Thaw on 25/11/2014.
//
//

#ifndef __kinectWrapper__kinectBlobWrapper__
#define __kinectWrapper__kinectBlobWrapper__

#include <stdio.h>
#include "ofMain.h"
#include "ofxOpenCv.h"

class BlobWrapper
{
    public:
        BlobWrapper(ofPoint _pos,float _wid, float _hei);
        void setup();
        void update();
        void draw(float _mod);
    
        void setTarget(ofPoint _pos);
    
        void setBlob(ofxCvBlob _blob);
    
        float strength;
        ofPoint pos;
        ofPoint target;
        ofPoint normalizedPos;
        ofPoint flipNormalizedPos;
        bool set;
    
        float depth;
        float depthPerc;
    
        ofRectangle rect;
        ofRectangle boundingBox;
        ofRectangle normalizedBoundingBox;
        ofRectangle flipNormalizedBoundingBox;
    
        ofxCvBlob blob;
        ofColor col;
};

#endif /* defined(__kinectWrapper__kinectBlobWrapper__) */
