//
//  headlines.h
//  octoBuild
//
//  Created by Jonathan Thaw on 03/12/2014.
//
//

#ifndef __octoBuild__headlines__
#define __octoBuild__headlines__

#include <stdio.h>
#include "ofMain.h"
#include "ofMain.h"
#include "ofxJSON.h"
#include "globals.h"
#include "ofxEasingFunc.h"

class HeadlineReader
{
    public:
        void setup();
        void update();
        void draw(int time);
        void fadeIn();
        int timeTracker;
    
        float destX;
        float destFinalX;
        
        ofxJSONElement data;
        ofxJSONElement holder[4];
    
        ofImage title;
        ofImage body;
        
        float wid;
        float setFrame;
        float timeFrame;
        float alpha;
        float graphicsAlpha;
    
        float switchStart;
        float switchTime;
        int switchPos;
    
        ofImage sucker;
};

#endif /* defined(__octoBuild__headlines__) */
