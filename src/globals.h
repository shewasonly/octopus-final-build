//
//  globals.h
//  octoBuild
//
//  Created by Jonathan Thaw on 27/11/2014.
//
//

#ifndef __octoBuild__globals__
#define __octoBuild__globals__

#include <stdio.h>
#include "ofMain.h"
#include "kinectBlobWrapper.h"
#include "ofxFTGL.h"
#include "ofxJSON.h"

class GlobalVars
{
public:
        static vector<BlobWrapper> kinectBlobs;
        static string screenName;
        static void debugGlobals(bool _debug);
        static void setup();
        static void draw(bool _debug);
        static void resize();
    
        static ofxFTGLSimpleLayout xsmallFont;
        static ofxFTGLSimpleLayout smallFont;
        static ofxFTGLSimpleLayout mediumFont;
        static ofxFTGLSimpleLayout largeFont;
        static ofxFTGLSimpleLayout xlargeFont;
    
        static ofColor mainColor;
    
        static ofxJSONElement settingsElem;
        static ofxJSONElement tubeJSON;
        static ofxJSONElement weatherJSON;
        static ofxJSONElement twitterJSON;
        static ofxJSONElement headlinesJSON;
    
        static ofRectangle screens[6];
        static bool isSet;
};

#endif /* defined(__octoBuild__globals__) */
