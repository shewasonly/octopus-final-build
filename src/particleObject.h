//
//  particleObject.h
//  orbitPLEASE
//
//  Created by Jonathan Thaw on 04/12/2014.
//
//

#ifndef __orbitPLEASE__particleObject__
#define __orbitPLEASE__particleObject__

#include <stdio.h>
#include "ofMain.h"

#define OUT_OF_ORBIT 0
#define CAPTURED_IN_ORBIT 1
#define EXITING_CENTRE 2
#define IN_ORBIT 3

class ParticleObject
{
    public:
        void setup(ofPoint _location, float _gravity, float _mass);
        void update(ofPoint centre, float _mass);
        void draw();
    
        void applyForce(ofPoint force);
    
        ofPoint location;
        ofPoint dispLocation[6];
        ofVec2f velocity;
        ofPoint acceleration;
    
        float gravity;
        float mass;
        float distance;
    
        ofPoint attract(ofPoint _location, float _mass,ofPoint _location2, float _mass2);
    
        int position;
        float gravitySet;
        ofPolyline path;
};

#endif /* defined(__orbitPLEASE__particleObject__) */
