//
//  commNodes.h
//  Triangulation
//
//  Created by Jonathan Thaw on 05/12/2014.
//
//

#ifndef __Triangulation__commNodes__
#define __Triangulation__commNodes__

#include <stdio.h>
#include "ofMain.h"

class CommunicationNode : public ofPoint {
    public:
        CommunicationNode(ofPoint last,float dist);
        void update();
        void draw();
    
        ofVec2f vel;
};

#endif /* defined(__Triangulation__commNodes__) */
