//
//  subdivision.cpp
//  subdivide
//
//  Created by Jonathan Thaw on 01/12/2014.
//
//

#include "subdivision.h"

void Subdivider::setup()
{
    isSetup = false;
    
    for(int i = 0; i < 6; i++)
    {
        AreaRect curr = init(i);
        areas.push_back(curr);
    }
    create();
    isSetup = true;
    
    nextColour = 1;
    currColour = 0;
}

AreaRect Subdivider::init(int i)
{
    int col = i%3;
    int row = 0;
    
    if(i>=3)
    {
        row = 1;
    }
    
    AreaRect curr;
    curr.shape = ofRectangle(col*(JTGetWindowWidth()/3), row*(JTGetWindowHeight()/2), JTGetWindowWidth()/3, JTGetWindowHeight()/2);
    curr.alpha = 0;
    curr.setFrame = 0;
    curr.waitFrame = 0;
    
    return curr;
}

void Subdivider::update(ofColor col)
{
    if(isSetup)
    {
        if(animating)
        {
            int still = 0;
            for(int i = 0; i < areas.size(); i++)
            {
                areas[i].update();
                
                if(areas[i].firing)
                {
                    still++;
                }
            }
            
            if(still==0&&animating)
            {
                animating = false;
                if(currColour==0)
                {
                    currColour = 1;
                    nextColour = 0;
                }
                else
                {
                    currColour = 0;
                    nextColour = 1;
                }
                
                create();
            }
        }
        else
        {
            float prob = ofRandom(100);
            
            if(prob>99)
            {
                animate();
            }
        }
    }
    
    colours[0] = col;
    colours[1] = ofColor(255,255,255);
}

void Subdivider::draw()
{
    if(isSetup)
    {
        ofSetColor(colours[currColour]);
        ofFill();
        ofRect(0, 0, JTGetWindowWidth(), JTGetWindowHeight());
        
        for(int i = 0; i < areas.size(); i++)
        {
            areas[i].draw(colours[nextColour]);
        }
    }
}

vector<AreaRect> Subdivider::create()
{
    areas.resize(6);
    
    for(int i = 0; i < 6; i++)
    {
        AreaRect curr = init(i);
        areas[i] = curr;
    }
    sub();
    
    return areas;
}

void Subdivider::sub()
{
    int rand = floor(ofRandom(0,areas.size()));
    
    int orientation = round(ofRandom(1));
    
    if(orientation==0) // width
    {
        float perc = 50;
        float wid = ofMap(perc,0,100,0,areas[rand].shape.width);
        
        
        if(wid>(JTGetWindowWidth()/50))
        {
            float ratio = areas[rand].shape.height/areas[rand].shape.width;
            
            if(ratio<1.5)
            {
                AreaRect curr;
                curr.shape = ofRectangle(areas[rand].shape.x + wid, areas[rand].shape.y, areas[rand].shape.width - wid, areas[rand].shape.height);
                curr.alpha = 0;
                curr.setFrame = 0;
                curr.waitFrame = 0;
                
                areas.push_back(curr);
                
                areas[rand].shape.width = wid;
            }
        }
    }
    else // height
    {
        float perc = 50;
        float hei = ofMap(perc,0,100,0,areas[rand].shape.height);
        
        if(hei>(JTGetWindowHeight()/50))
        {
            float ratio = areas[rand].shape.width/areas[rand].shape.height;
            
            if(ratio<1.5)
            {
                AreaRect curr;
                curr.shape = ofRectangle(areas[rand].shape.x, areas[rand].shape.y + hei, areas[rand].shape.width, areas[rand].shape.height - hei);
                curr.alpha = 0;
                curr.setFrame = 0;
                curr.waitFrame = 0;
                
                areas.push_back(curr);
                
                areas[rand].shape.height = hei;
            }
        }
    }
    
    if(areas.size()<200)
    {
        if(areas.size()<100)
        {
            sub();
        }
        else
        {
            float prob = ofRandom(100);
            
            if(prob>10)
            {
                sub();
            }
        }
    }
}

void Subdivider::animate()
{
    if(!animating)
    {
        animating = true;
        
        int rand = floor(ofRandom(0,areas.size()));
        areas[rand].fire(0.1);
        
        for(int i = 0; i < areas.size(); i++)
        {
            areas[i].alpha = 0;
            if(i!=rand)
            {
                areas[i].fire(ofRandom(10,30));
            }
        }
    }
}

void Subdivider::paintArea(ofRectangle check)
{
    if(animating)
    {
        for(int i = 0; i < areas.size(); i++)
        {
            for(int x = round(check.x); x < check.x+check.width; x++)
            {
                for(int y = round(check.y); y < check.y+check.height; y = y + 20)
                {
                    if(ofInRange(x, areas[i].shape.getX(), areas[i].shape.getX() + areas[i].shape.getWidth()))
                    {
                        if(ofInRange(y, areas[i].shape.getY(), areas[i].shape.getY() + areas[i].shape.getHeight()))
                        {
                            areas[i].timeWait = 1;
                        }
                    }
                }
            }
        }
    }
}





