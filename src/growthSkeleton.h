//
//  growthSkeleton.h
//  growthNext
//
//  Created by Jonathan Thaw on 04/12/2014.
//
//

#ifndef __growthNext__growthSkeleton__
#define __growthNext__growthSkeleton__

#include <stdio.h>
#include "ofMain.h"
#include "ofxEasingFunc.h"
#include "growthBar.h"

struct Bar {
    bool master;
    float actualPerc;
    float setFrame;
    GrowthBar bar;
};

class GrowthSkeleton
{
    public:
        void setup(int amt);
        void update();
        void draw();
    
        void randomFire();
        void increase(int index, float perc);
        vector<Bar> barPoints;
    
        int barAmt;
    
        vector<int> randomShapes[4];
        int randomSet;
    
        ofImage img;
};

#endif /* defined(__growthNext__growthSkeleton__) */
