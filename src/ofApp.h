#pragma once

#include "ofMain.h"

#include "globals.h"

#include "JTTimeline.h"

#include "kinectWrapper.h"
#include "kinectBlobWrapper.h"

#include "jsonHandling.h"

#include "infoScreens.h"

#include "visualScreens.h"

#include "transitions.h"

#include "clock.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);
        
        void exit();
        
        bool debugMode;
        
        //Timeline Functions and variables -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
            JTTimeline timeline;
            void switchStage(int &val);
            int stageVal;
            int loopNum;
    
        //Screen decisions -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
            void randomScreens();
            int screensPos;
            int screensArr[4];
    
    
        //Kinect wrapper -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
            KinectWrap kinectCam;
            vector<BlobWrapper> kinectBlobs;
    
        // JSON handling -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
            jsonHandler json;
            void updateJSON(int &val);
    
    
        // Info screens -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
            InfoScreens info;
        
        // Visual screens -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
            VisualScreens visual;
        
        // Transition screens -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
            Transitioner transition;
            float percTrans;
		
        // Clock screen -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
            ClockDisplay clock;
};
