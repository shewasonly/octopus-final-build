//
//  weatherAnimation.cpp
//  octoBuild
//
//  Created by Jonathan Thaw on 27/11/2014.
//
//

#include "weatherAnimation.h"

void WeatherAnim::setup(string _weather, float _wid, float _hei)
{
    width = _wid;
    height = _hei;
    weather = _weather;
    setFrame = ofGetFrameNum();
    timeInterval = 5;
    
    if(weather=="Rain"||weather=="Drizzle")
    {
        for(int i = 0; i < 20; i ++)
        {
            rainDrops.push_back(ofRectangle(0.0,0.0,0.0,10));
        }
    }
    
    if(weather=="Snow")
    {
        for(int i = 0; i < 20; i ++)
        {
            rainDrops.push_back(ofRectangle(0.0,0.0,0.0,5));
        }
    }
}

void WeatherAnim::update()
{
    if(weather=="Rain"||weather=="Drizzle")
    {
        if(setFrame+timeInterval<ofGetFrameNum())
        {
            int fired = 0;
            setFrame = ofGetFrameNum();
            for(int i = 0; i < rainDrops.size(); i ++)
            {
                if(rainDrops[i].width==0.0)
                {
                    rainDrops[i].x = ofRandom(width);
                    rainDrops[i].y = 0.0;
                    rainDrops[i].width = 1.0;
                    rainDrops[i].height = 2.0;
                    break;
                }
            }
            
        }
        
        for(int i = 0; i < rainDrops.size(); i ++)
        {
            if(rainDrops[i].width==1.0)
            {
                rainDrops[i].y += rainDrops[i].height;
                
                rainDrops[i].height += rainDrops[i].height * .5;
                
                rainDrops[i].height = ofClamp(rainDrops[i].height, 4, 30);
                
                if(rainDrops[i].y > height)
                {
                    rainDrops[i].width = 0.0;
                }
            }
        }
    }

    if(weather=="Snow")
    {
        if(setFrame+timeInterval<ofGetFrameNum())
        {
            int fired = 0;
            setFrame = ofGetFrameNum();
            for(int i = 0; i < rainDrops.size(); i ++)
            {
                if(rainDrops[i].width==0.0)
                {
                    rainDrops[i].x = ofRandom(width);
                    rainDrops[i].y = 0.0;
                    rainDrops[i].width = 1.0;
                    rainDrops[i].height = 2.0;
                    break;
                }
            }
            
        }
        
        for(int i = 0; i < rainDrops.size(); i ++)
        {
            if(rainDrops[i].width==1.0)
            {
                rainDrops[i].y += rainDrops[i].height;
                
                rainDrops[i].height += rainDrops[i].height * .5;
                
                rainDrops[i].height = ofClamp(rainDrops[i].height, 4, 5);
                
                if(rainDrops[i].y > height)
                {
                    rainDrops[i].width = 0.0;
                }
            }
        }
    }

    if(weather=="Clouds")
    {
        if(!clouds.bAllocated())
        {
            clouds.allocate(width,height,OF_IMAGE_COLOR_ALPHA);
            
            for(unsigned x = 0; x < width; x++)
            {
                for(unsigned y = 0; y < height; y++)
                {
                    float alpha = ofMap( y, 0, height, 220, 50);
                    clouds.setColor(x, y, ofColor(100,100,100,alpha));
                }
            }
            
            clouds.update();
        }
    }
}

void WeatherAnim::draw()
{
    if(weather=="Rain"||weather=="Drizzle")
    {
        for(int i = 0; i < rainDrops.size(); i ++)
        {
            if(rainDrops[i].width==1.0)
            {
                ofPoint curr(rainDrops[i].x,rainDrops[i].y);
                
                ofSetColor(0,0,255);
                ofFill();
                ofRect(curr, 2, 10);
            }
        }
    }
    if(weather=="Snow")
    {
        for(int i = 0; i < rainDrops.size(); i ++)
        {
            if(rainDrops[i].width==1.0)
            {
                ofPoint curr(rainDrops[i].x,rainDrops[i].y);
                
                ofSetColor(255);
                ofFill();
                ofEllipse(curr, 5, 5);
            }
        }
    }
    
    if(weather=="Clouds")
    {
        if(clouds.bAllocated())
        {
            ofSetColor(100,100,100,200);
            ofFill();
            clouds.draw(0,0, width, height);
        }
    }
}

void WeatherAnim::setWeather(string _weather)
{
    if(_weather!=oldWeather)
    {
        oldWeather = _weather;
        setup(_weather,width,height);
    }
}