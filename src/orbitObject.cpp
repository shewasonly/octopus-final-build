//
//  orbitObject.cpp
//  orbitPLEASE
//
//  Created by Jonathan Thaw on 04/12/2014.
//
//

#include "orbitObject.h"

void OrbitObject::setup(ofPoint _location, float _gravity, float _mass)
{
    gravitySet = false;
    location = _location;
    gravity = _gravity;
    mass = _mass;
    
    velocity.x = 5;
    velocity.y = 5;
    
    for(int i = 0; i < 500; i++)
    {
        ParticleObject curr;
        curr.setup(ofPoint(ofRandom(JTGetWindowWidth()),ofRandom(JTGetWindowHeight())), gravity, ofRandom(3,5));
        particle.push_back(curr);
    }
}

void OrbitObject::update()
{
    if(location.x<0||location.x>JTGetWindowWidth())
    {
        velocity.x *= -1;
        location.x = ofClamp(location.x, 0, JTGetWindowWidth());
    }
    if(location.y<0||location.y>JTGetWindowHeight())
    {
        velocity.y *= -1;
        location.y = ofClamp(location.y, 0, JTGetWindowHeight());
    }
    
    if(!gravitySet)
    {
        location += velocity;
    }
    
    for(int i = 0; i < particle.size(); i++)
    {
        particle[i].update(location,mass);
    }
}

void OrbitObject::draw()
{
    for(int i = 0; i < particle.size(); i++)
    {
        ofSetColor(255);
        ofNoFill();
        particle[i].draw();
    }
}

ofPoint OrbitObject::attract(ofPoint _location, float _mass,ofPoint _location2, float _mass2)
{
    ofPoint force = _location - _location2;
    float distance = force.length();
    distance = ofClamp(distance,1.0,5.0);
    force.normalize();
    
    float strength = (gravity * _mass * _mass2) / (distance * distance);
    force.scale(strength);
    
    return force;
}

void OrbitObject::setGravity()
{
    if(gravitySet)
    {
        gravitySet = false;
        gravity = 0.05;
    }
    else
    {
        gravitySet = true;
        gravity = 1.5;
    }
    for(int i = 0; i < particle.size(); i++)
    {
        particle[i].gravity = gravity;
    }
}