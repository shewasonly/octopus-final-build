//
//  growthSkeleton.cpp
//  growthNext
//
//  Created by Jonathan Thaw on 04/12/2014.
//
//

#include "growthSkeleton.h"

void GrowthSkeleton::setup(int amt)
{
    randomSet = 0;
    barAmt = amt;
    
    float wid = (float) JTGetWindowWidth() / ((barAmt*4)-3);
    float hei = JTGetWindowHeight();
    
    img.allocate(wid,hei,OF_IMAGE_COLOR_ALPHA);
    for(unsigned x = 0; x < wid; x++)
    {
        for(unsigned y = 0; y < hei; y++)
        {
            float alpha = ofMap( y, 0, hei, 0,255);
            img.setColor(x, y, ofColor(255,255,255,alpha));
        }
    }
    img.update();
    
    for(int i = 0; i < (barAmt*4)-3; i++)
    {
        Bar curr;
        
        if(i%4==0)
        {
            curr.master = true;
        }
        else
        {
            curr.master = false;
        }
        curr.actualPerc = 0;
        curr.setFrame = 0;
        curr.bar.setup(wid,hei,img);
        
        barPoints.push_back(curr);
    }
    
    // DEFINING RANDOM SHAPES
    
    for(int i = 0; i < (barAmt*4)-3; i++)  // ascending
    {
        float inc = ofMap(i,0,(barAmt*4)-3,0,100);
        randomShapes[0].push_back(inc);
    }
    for(int i = 0; i < (barAmt*4)-3; i++)  // descending
    {
        float inc = ofMap(i,0,(barAmt*4)-3,100,0);
        randomShapes[1].push_back(inc);
    }
    for(int i = 0; i < (barAmt*4)-3; i++)  // descending
    {
        float angle = ofMap(i,0,(barAmt*4)-3,0,360);
        float pos = ofMap(sin(angle),-1,1,50,100);
        randomShapes[2].push_back(pos);
    }
    for(int i = 0; i < (barAmt*4)-3; i++)  // descending
    {
        if(i<((barAmt*4)-3)/2)
        {
            float inc = ofMap(i,0,(barAmt*4)-3,0,200);
            randomShapes[3].push_back(inc);
        }
        else
        {
            float inc = ofMap(i,0,(barAmt*4)-3,200,0);
            randomShapes[3].push_back(inc);
        }
    }
}

void GrowthSkeleton::update()
{
    for(int i = 0; i < barPoints.size(); i++)
    {
        if(barPoints[i].master)
        {
            barPoints[i].actualPerc -= 2;
            barPoints[i].actualPerc = ofClamp(barPoints[i].actualPerc,0,100);
            barPoints[i].bar.setHeight(barPoints[i].actualPerc);
            barPoints[i].bar.update();
        }
        else
        {
            barPoints[i].actualPerc = ofMap(50,0,100,barPoints[i-1].actualPerc,barPoints[i+1].actualPerc);
            barPoints[i].bar.setHeight(barPoints[i].actualPerc);
            barPoints[i].bar.update();
        }
    }
    
    float prob = ofRandom(100);
    if(prob>99&&randomSet+(60*5)<ofGetFrameNum())
    {
        randomFire();
    }
}

void GrowthSkeleton::draw()
{
    float wid = (float) JTGetWindowWidth() / barPoints.size();
    float left = 0;
    
    ofRectMode(OF_RECTMODE_CENTER);
    
    ofPushMatrix();
    
    for(int i = 0; i < barPoints.size(); i++)
    {
        barPoints[i].bar.draw(wid);
        ofTranslate(wid, 0);
    }
    
    ofPopMatrix();
}

void GrowthSkeleton::increase(int index, float perc)
{
    if(barPoints[index].master)
    {
        barPoints[index].actualPerc = perc;
    }
}

void GrowthSkeleton::randomFire()
{
    randomSet = ofGetFrameNum();
    
    int rand = floor(ofRandom(4));
    
    for(int i = 0; i < randomShapes[rand].size(); i++)
    {
        increase(i,randomShapes[rand][i]);
    }
}





