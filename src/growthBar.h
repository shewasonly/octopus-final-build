//
//  growthBar.h
//  growthNext
//
//  Created by Jonathan Thaw on 04/12/2014.
//
//

#ifndef __growthNext__growthBar__
#define __growthNext__growthBar__

#include <stdio.h>
#include "ofMain.h"
#include "ofxEasingFunc.h"

class GrowthBar
{
public:
    void setup(float wid, float hei, ofImage &img);
    void update();
    void draw(float wid);
    
    void setHeight(float perc);
    
    float heightPerc;
    float targetHeightPerc;
    
    float rand;
    float alter;
    
    float tempHei;
    
    ofImage image;
};

#endif /* defined(__growthNext__growthBar__) */
