//
//  weatherAnimation.h
//  octoBuild
//
//  Created by Jonathan Thaw on 27/11/2014.
//
//

#ifndef __octoBuild__weatherAnimation__
#define __octoBuild__weatherAnimation__

#include <stdio.h>
#include "ofMain.h"
#include "globals.h"

class WeatherAnim
{
    public:
        void setup(string _weather, float _wid, float _hei);
        void update();
        void draw();
    
        void setWeather(string _weather);
    
        string weather;
        string oldWeather;
    
        vector<ofRectangle> rainDrops;
    
        int setFrame;
        int timeInterval;
    
        float width;
        float height;
    
        ofImage clouds;
};

#endif /* defined(__octoBuild__weatherAnimation__) */
