//
//  weatherIndividual.h
//  octoBuild
//
//  Created by Jonathan Thaw on 27/11/2014.
//
//

#ifndef __octoBuild__weatherIndividual__
#define __octoBuild__weatherIndividual__

#include <stdio.h>
#include "ofMain.h"
#include "globals.h"
#include "ofxJSON.h"
#include "weatherAnimation.h"
#include "ofxEasingFunc.h"

class WeatherIndividual
{
    public:
        void setup(float _wid, float _hei, int _index);
        void update();
        void draw();
        void fadeIn();
        
        float width;
        float height;
        int index;
    
        float setFrame;
        float timeFrame;
        float alpha;
    
        ofxJSONElement data;
        
        WeatherAnim anim;
};

#endif /* defined(__octoBuild__weatherIndividual__) */
