//
//  twitter.h
//  octoBuild
//
//  Created by Jonathan Thaw on 28/11/2014.
//
//

#ifndef __octoBuild__twitter__
#define __octoBuild__twitter__

#include <stdio.h>
#include "ofMain.h"
#include "ofxJSON.h"
#include "globals.h"
#include "ofxEasingFunc.h"

class TwitterScreen
{
    public:
        void setup();
        void update();
        void draw(int time);
        void fadeIn();
    
        int timeTracker;
    
        float destX;
        float destFinalX;
    
        ofxJSONElement data;
    
        ofImage title;
        ofImage body;
    
        float wid;
        float setFrame;
        float timeFrame;
        float alpha;
        float graphicsAlpha;
    
        ofVec4f bezier[3];
        ofVec4f bezierVel[3];
        ofPath bg[3];
};

#endif /* defined(__octoBuild__twitter__) */
