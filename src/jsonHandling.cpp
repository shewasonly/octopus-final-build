//
//  jsonHandling.cpp
//  octopusSkeleton
//
//  Created by Jonathan Thaw on 13/11/2014.
//
//

#include "jsonHandling.h"

void jsonHandler::setup()
{
    connected = isConnected();
}

void jsonHandler::threadedFunction() {
    
    while(isThreadRunning()) {
        
        //WELCOME TO THE THROTTLE ZONE
        int frameNum = ofGetFrameNum();
        
        if(frameNum%600==0) // once every 10 seconds
        {
            update(false);
        }
        if(frameNum%900==0) // once every quarter minute
        {
            update(true);
        }
    }
}

bool jsonHandler::isConnected()
{
    return pingTest.open("http://dev.shewasonly.co.uk/octopus_api/pingTest.json");
}

void jsonHandler::update(bool full)
{
    if(!full)
    {
        connected = isConnected();
    }
    else
    {
        grabFeeds();
    }
}

void jsonHandler::grabFeeds()
{
    if(connected)
    {
        cout<<ofToString(getTime())<<" Beginning feed grab"<<endl;
        
        string tubesURL = "http://dev.shewasonly.co.uk/octopus_api/tfl_lines.json";
        bool tubeConnect = tubes.open(tubesURL);
        
        if(tubeConnect)
        {
            lock();
            tubes.getRawString(true);
            unlock();
            
            cout<<" -- "<<ofToString(getTime())<<" Tube data received"<<endl;
            saveJSON("JSON/tfl_lines.json",tubes);
        }
        
        string weatherURL = "http://dev.shewasonly.co.uk/octopus_api/weather.json";
        bool weatherConnect = weather.open(weatherURL);
        
        if(weatherConnect)
        {
            lock();
            weather.getRawString(true);
            unlock();
            
            cout<<" -- "<<ofToString(getTime())<<" Weather data received"<<endl;
            saveJSON("JSON/weather.json",weather);
        }
        
        string twitterURL = "http://dev.shewasonly.co.uk/octopus_api/twitter.json";
        bool twitterConnect = twitter.open(twitterURL);
        
        if(twitterConnect)
        {
            lock();
            twitter.getRawString(true);
            unlock();
            
            cout<<" -- "<<ofToString(getTime())<<" Twitter data received"<<endl;
            saveJSON("JSON/twitter.json",twitter);
        }
        
        string headlinesURL = "http://dev.shewasonly.co.uk/octopus_api/headlines.json";
        bool headlinesConnect = headlines.open(headlinesURL);
        
        if(headlinesConnect)
        {
            lock();
            headlines.getRawString(true);
            unlock();
            
            cout<<" -- "<<ofToString(getTime())<<" Headlines data received"<<endl;
            saveJSON("JSON/headlines.json",headlines);
        }
        
        cout<<" "<<endl;
    }
    else
    {
        cout<<ofToString(getTime())<<" Beginning feed grab (no connection)"<<endl;
        
        string tubesURL = "JSON/tfl_lines.json";
        bool tubeConnect = tubes.open(tubesURL);
        
        if(tubeConnect)
        {
            lock();
            tubes.getRawString(true);
            unlock();
            
            cout<<" -- "<<ofToString(getTime())<<" Tube data received"<<endl;
        }
        
        string weatherURL = "JSON/weather.json";
        bool weatherConnect = weather.open(weatherURL);
        
        if(weatherConnect)
        {
            lock();
            weather.getRawString(true);
            unlock();
            
            cout<<" -- "<<ofToString(getTime())<<" Weather data received"<<endl;
        }
        
        string twitterURL = "JSON/twitter.json";
        bool twitterConnect = twitter.open(twitterURL);
        
        if(twitterConnect)
        {
            lock();
            twitter.getRawString(true);
            unlock();
            
            cout<<" -- "<<ofToString(getTime())<<" Twitter data received"<<endl;
        }
        
        string headlinesURL = "JSON/headlines.json";
        bool headlinesConnect = headlines.open(headlinesURL);
        
        if(headlinesConnect)
        {
            lock();
            headlines.getRawString(true);
            unlock();
            
            cout<<" -- "<<ofToString(getTime())<<" Headlines data received"<<endl;
            saveJSON("JSON/headlines.json",headlines);
        }
        
        cout<<" "<<endl;
    }
    sendStageEvent();
}

void jsonHandler::saveJSON(string path, ofxJSONElement data)
{
    ofFile newfile(ofToDataPath(path), ofFile::WriteOnly);
    newfile << data;
    cout<<" -- "<<ofToString(getTime())<<" "<<path<<" was saved"<<endl;
}

ofxJSONElement jsonHandler::returnFeed(string feed)
{
    if(feed == "tube")
    {
        return tubes;
    }
    
    if(feed == "weather")
    {
        return weather;
    }
    
    if(feed == "twitter")
    {
        return twitter;
    }
    
    if(feed == "headlines")
    {
        return headlines;
    }
}

string jsonHandler::getTime()
{
    string ret = ofToString(ofGetHours());
    ret += ":";
    ret += ofToString(ofGetMinutes());
    ret += ":";
    ret += ofToString(ofGetSeconds());
    
    return ret;
}

void jsonHandler::sendStageEvent()
{
    int i = 1;
    lock();
    ofNotifyEvent(jsonNotify, i, this);
    unlock();
}



