//
//  communicationBody.cpp
//  Triangulation
//
//  Created by Jonathan Thaw on 05/12/2014.
//
//

#include "communicationBody.h"

void CommunicationBody::setup(int max)
{
    maxAmt = ofClamp(max,10,1000);
    recycle = 0;
    ofSetFrameRate(60);
    lastPoint = ofPoint(ofRandom(0,JTGetWindowWidth()),ofRandom(0,JTGetWindowHeight()));
    nodes.reserve(maxAmt);
    
    for(int i = 0; i<10; i++)
    {
        addPoint(lastPoint,100);
        triangulation.addPoint(nodes[nodes.size()-1]);
        triangulation.triangulate();
    }
}

void CommunicationBody::update()
{
    float rand = ofRandom(100);
    
    if(rand>95)
    {
        addPoint(lastPoint, JTGetWindowHeight()*0.4);
    }
    
    triangulation.reset();
    for(int i = 0; i<nodes.size(); i++)
    {
        nodes[i].update();
        triangulation.addPoint(nodes[i]);
    }
    triangulation.triangulate();
}

void CommunicationBody::draw()
{
    ofSetColor(255);
    ofNoFill();
    triangulation.draw();
    for(int i = 0; i<nodes.size(); i++)
    {
        ofSetColor(100, 0, 0);
        ofFill();
        nodes[i].draw();
    }
    /*
    ofSetColor(0);
    ofFill();
    ofDrawBitmapString("Nodes: "+ofToString(nodes.size())+"\n"+"FrameRate: "+ofToString(ofGetFrameRate()), 10,20);
     */
}

void CommunicationBody::addPoint(ofPoint pos, float strength)
{
    if(ofInRange(pos.x,0,JTGetWindowWidth())&&ofInRange(pos.y,0,JTGetWindowHeight()))
    {
        if(nodes.size()>=maxAmt)
        {
            if(recycle<maxAmt)
            {
                nodes[recycle] = CommunicationNode(pos,strength);
                recycle++;
            }
            else
            {
                nodes[0] = CommunicationNode(pos,strength);
                recycle = 1;
                lastPoint = nodes[nodes.size()-1];
            }
        }
        else
        {
            for(int i = 0; i<1; i++)
            {
                nodes.push_back(CommunicationNode(pos,strength));
            }
        }
    }
    
    lastPoint = nodes[nodes.size()-1];
}