//
//  kinectBlobWrapper.cpp
//  kinectWrapper
//
//  Created by Jonathan Thaw on 25/11/2014.
//
//

#include "kinectBlobWrapper.h"

BlobWrapper::BlobWrapper(ofPoint _pos,float _wid, float _hei)
{
    pos = _pos;
    target = pos;
    strength = 0.1;
    set = false;
    setup();
    rect.set(0, 0, _wid, _hei);
    boundingBox.set(0,0,1,1);
}

void BlobWrapper::setup()
{
    
}

void BlobWrapper::update()
{
    float dist = ofDist(pos.x,pos.y,target.x,target.y);
    
    if(dist>15)
    {
        pos.interpolate(target, strength);
    }
    
    normalizedPos.x = ofMap(pos.x,0,rect.getWidth(),0,JTGetWindowWidth());
    normalizedPos.y = ofMap(pos.y,0,rect.getHeight(),0,JTGetWindowHeight());
    flipNormalizedPos.x = ofMap(pos.x,0,rect.getWidth(),JTGetWindowWidth(),0);
    flipNormalizedPos.y = ofMap(pos.y,0,rect.getHeight(),0,JTGetWindowHeight());
    
    normalizedBoundingBox.setWidth(ofMap(boundingBox.getWidth(),0,rect.getWidth(),0,JTGetWindowWidth()));
    normalizedBoundingBox.setHeight(ofMap(boundingBox.getHeight(),0,rect.getHeight(),0,JTGetWindowHeight()));
    normalizedBoundingBox.setX(ofMap(boundingBox.getX(),0,rect.getWidth(),0,JTGetWindowWidth()));
    normalizedBoundingBox.setY(ofMap(boundingBox.getY(),0,rect.getHeight(),0,JTGetWindowHeight()));
}

void BlobWrapper::draw(float _mod)
{
    ofPoint curr = pos / _mod;
    ofSetColor(0,255,0);
    ofFill();
    ofEllipse(curr, 5, 5);
    
    ofLine(curr.x - 15, curr.y, curr.x + 15, curr.y);
    ofLine(curr.x, curr.y - 15, curr.x, curr.y + 15);
    
    ofDrawBitmapString("Depth: "+ofToString(depth), curr + ofPoint(10,10));
}

void BlobWrapper::setTarget(ofPoint _pos)
{
    if(!set)
    {
        set = true;
        col = ofColor(ofRandom(255),ofRandom(255),ofRandom(255));
        pos = _pos;
    }
    target = _pos;
}

void BlobWrapper::setBlob(ofxCvBlob _blob)
{
    blob = _blob;
}