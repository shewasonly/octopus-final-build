//
//  infoScreens.h
//  octoBuild
//
//  Created by Jonathan Thaw on 27/11/2014.
//
//

#ifndef __octoBuild__infoScreens__
#define __octoBuild__infoScreens__

#include <stdio.h>
#include "ofMain.h"
#include "globals.h"
#include "twitter.h"
#include "headlines.h"
#include "weatherScreens.h"


class InfoScreens
{
    public:
        void setup();
        void update();
        void draw(int loopTime);
        void debugFire();
    
        void begin();
    
        void resize();
    
        float startFrame;
        float timeFrame;
        int infoPos;
    
        int screenOrder[3];
        float screenTimes[3];
        int loopAmount;
    
        // Screens -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
            TwitterScreen twitter;
    
            HeadlineReader headlines;
    
            WeatherScreens weather;
};

#endif /* defined(__octoBuild__infoScreens__) */
