//
//  transitions.cpp
//  TimelineThread
//
//  Created by Jonathan Thaw on 05/12/2014.
//
//

#include "transitions.h"

void Transitioner::setup()
{
    playing = 0;
    transitionVid.setPixelFormat(OF_PIXELS_RGBA);
    transitionVid.loadMovie("TRANSITIONS/960.mov");
    transitionVid.setLoopState(OF_LOOP_NONE);
    transitionVid.play();
}

float Transitioner::update()
{
    transitionVid.update();
    return transitionVid.getPosition();
}

void Transitioner::draw()
{
    ofSetColor(255);
    ofFill();
    transitionVid.draw(0, 0, JTGetWindowWidth(), JTGetWindowHeight());
}

void Transitioner::play()
{
    transitionVid.firstFrame();
    transitionVid.play();
}