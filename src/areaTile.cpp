//
//  areaTile.cpp
//  subdivide
//
//  Created by Jonathan Thaw on 01/12/2014.
//
//

#include "areaTile.h"

void AreaRect::update()
{
    if(firing)
    {
        float t = ofMap(ofGetElapsedTimef(),setFrame,setFrame + timeWait,0, 1);
        
        // all functions take input 0. ~ 1., and outputs 0. ~ 1.
        float v = ofxEasingFunc::Cubic::easeIn(t);
        
        float here = ofMap(v,0,1,0,255);
        float prob = ofRandom(100);
        
        if(prob>99)
        {
            alpha = here;
        }
        
        if(here>254)
        {
            alpha = 255;
            firing = false;
        }
    }
}

void AreaRect::draw(ofColor col)
{
    ofSetColor(col.r,col.g,col.b,alpha);
    ofFill();
    ofRect(shape);
}

void AreaRect::fire(float _time)
{
    if(!firing)
    {
        setFrame = ofGetElapsedTimef();
        timeWait = _time;
        firing = true;
    }
}










