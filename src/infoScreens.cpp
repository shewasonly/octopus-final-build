//
//  infoScreens.cpp
//  octoBuild
//
//  Created by Jonathan Thaw on 27/11/2014.
//
//

#include "infoScreens.h"

void InfoScreens::setup()
{
    timeFrame = 20;
    infoPos = 0;
    
    loopAmount = 0;
    
    screenOrder[0] = 0;
    screenTimes[0] = 50;
    
    screenOrder[1] = 1;
    screenTimes[1] = 20;
    
    screenOrder[2] = 2;
    screenTimes[2] = 20;
    
    weather.setup();
    twitter.setup();
    headlines.setup();
}

void InfoScreens::update()
{
    if(startFrame+screenTimes[infoPos]<ofGetElapsedTimef())
    {
        infoPos++;
        startFrame = ofGetElapsedTimef();
        
        if(infoPos>2)
        {
            infoPos = 0;
            loopAmount++;
        }
    }
    weather.update();
    twitter.update();
    headlines.update();
}

void InfoScreens::draw(int loopTime)
{
    switch(screenOrder[infoPos])
    {
        case 0:
            headlines.draw(loopTime);
            break;
        case 1:
            twitter.draw(loopTime);
            break;
        case 2:
            weather.draw(loopTime);
            break;
    }
    
    string hour = ofToString(ofGetHours());
    
    if(hour.size()==1)
    {
        hour.insert(0,"0");
    }
    
    string minute = ofToString(ofGetMinutes());
    
    if(minute.size()==1)
    {
        minute.insert(0,"0");
    }
    
    ofPushMatrix();
    ofTranslate(JTGetWindowWidth() - 100, JTGetWindowHeight()-70);
    GlobalVars::xsmallFont.formatText(hour+":"+minute,JTGetWindowWidth());
    ofPopMatrix();
}

void InfoScreens::debugFire()
{
    
}

void InfoScreens::begin()
{
    infoPos = 0;
    startFrame = ofGetElapsedTimef();
}


void InfoScreens::resize()
{
    headlines.wid = JTGetWindowWidth() * 0.75;
    twitter.wid = JTGetWindowWidth() * 0.75;
}