//
//  orbitObject.h
//  orbitPLEASE
//
//  Created by Jonathan Thaw on 04/12/2014.
//
//

#ifndef __orbitPLEASE__orbitObject__
#define __orbitPLEASE__orbitObject__

#include <stdio.h>
#include "ofMain.h"
#include "particleObject.h"

class OrbitObject
{
    public:
        void setup(ofPoint _location, float _gravity, float _mass);
        void update();
        void draw();
        
        ofPoint location;
        ofPoint velocity;
        ofPoint acceleration;
        
        float gravity;
        float mass;
    
        vector<ParticleObject> particle;
    
        ofPoint attract(ofPoint _location, float _mass,ofPoint _location2, float _mass2);
    
        void setGravity();
    
        float gravitySet;
};

#endif /* defined(__orbitPLEASE__orbitObject__) */
