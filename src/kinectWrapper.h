//
//  kinectWrapper.h
//  kinectWrapper
//
//  Created by Jonathan Thaw on 25/11/2014.
//
//

#ifndef __kinectWrapper__kinectWrapper__
#define __kinectWrapper__kinectWrapper__

#include <stdio.h>
#include "ofMain.h"
#include "globals.h"

#include "ofxOpenCv.h"
#include "ofxKinect.h"
#include "kinectBlobWrapper.h"
#include "ofxUI.h"
#include "ofxXmlSettings.h"


class KinectWrap
{
    public:
        void start(bool _debug);
        void update();
        void draw(int x, int y);
        void end();
        void guiEvent(ofxUIEventArgs &e);
        void setDebug(bool mode);
    
        void bgSnap();
    
        int maxBlobs;
    
        ofxKinect kinect;
        
        ofxCvGrayscaleImage grayImage; // Used for conversion to grayscale
        ofxCvGrayscaleImage grayThreshNear; // Near threshold to diff
        ofxCvGrayscaleImage grayThreshFar; // Far threshold to diff
        int nearThreshold;
        int farThreshold;
    
        ofxCvGrayscaleImage blurred; // Smooth that image so its not as harsh
        ofxCvGrayscaleImage background; // This is the background to base difference on
        ofxCvGrayscaleImage diff; // This is how we find if anything is different to bg
        ofxCvGrayscaleImage mask; // Then we can alter that difference to make it more binary
        ofxCvGrayscaleImage straightMask; // Then we can alter that difference to make it more binary
    
        ofxCvContourFinder 	contourFinder;
        ofxCvContourFinder 	straightContourFinder;
    
        vector<ofPoint> obj;	//object's centers
    
        vector<BlobWrapper> myBlobs;
        void returnBlobs();
    
        ofMesh mesh;
    
        bool drawIt;
        bool debugMode;
    
        ofxUICanvas *gui;
    
        int farDepth;
        int nearDepth;
};


#endif /* defined(__kinectWrapper__kinectWrapper__) */
