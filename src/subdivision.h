//
//  subdivision.h
//  subdivide
//
//  Created by Jonathan Thaw on 01/12/2014.
//
//

#ifndef __subdivide__subdivision__
#define __subdivide__subdivision__

#include <stdio.h>
#include "ofMain.h"
#include "ofxEasingFunc.h"
#include "areaTile.h"

class Subdivider
{
    public:
        void setup();
        void update(ofColor col);
        void draw();
    
        vector<AreaRect> create();
        void sub();
    
        void animate();
        bool animating;
    
        void paintArea(ofRectangle check);
    
        int columns;
        int rows;
    
        vector<AreaRect> areas;
    
        AreaRect init(int i);
    
        bool dividing;
    
        bool isSetup;
    
        ofColor colours[2];
    
        int currColour;
        int nextColour;
};

#endif /* defined(__subdivide__subdivision__) */
