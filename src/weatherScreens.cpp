//
//  weatherScreens.cpp
//  octoBuild
//
//  Created by Jonathan Thaw on 27/11/2014.
//
//

#include "weatherScreens.h"

void WeatherScreens::setup()
{
    timeTracker = -1;
    for(int i = 0; i <3; i++)
    {
        weather[i].setup(GlobalVars::screens[0].width, JTGetWindowHeight(),i);
    }
}

void WeatherScreens::update()
{
    for(int i = 0; i <3; i++)
    {
        weather[i].update();
    }
}

void WeatherScreens::draw(int time)
{
    if(time!=timeTracker)
    {
        for(int i = 0; i <3; i++)
        {
            weather[i].fadeIn();
        }
        timeTracker = time;
    }
    ofPushMatrix();
    for(int i = 0; i <3; i++)
    {
        weather[i].draw();
        ofTranslate(JTGetWindowWidth()/3,0);
    }
    ofPopMatrix();
}