//
//  JTTimeline.h
//  TimelineThread
//
//  Created by Jonathan Thaw on 26/11/2014.
//
//

#pragma once
#ifndef __TimelineThread__JTTimeline__
#define __TimelineThread__JTTimeline__

#include <stdio.h>
#include "ofMain.h"

class JTTimeline : public ofThread
{
    public:
        void threadedFunction();
        void setup(bool _debug);
        void update();
        void draw();
        
        bool debugMode;
    
        string readable();
    
        void debugStr();
    
        void setVariables();
        void updateVariables();
    
        float timeToMillis(int _hours, int _minutes, int _seconds);
    
        ofColor returnColor(string selector);
    
        float totalTime;
        float currentTime;
        float baseTime;
        int currHour;
        int currMin;
        int currSecond;
    
        float lastTime;
    
        int coloursAmt;
        ofColor mainColours[9];
        ofColor mainColour;
        float colourTimes[10];
        int currColour;
        int nextColour;
        int dayCount;
    
    
        int stagesAmt;
        int stageValues[4];
        int stagePos;
        float stageTimes[3][4];
        int currStage;
        int oldStage;
        int stageTick;
        int stageTickPos[3];
    
        ofEvent<int> stageNotify;
        void sendStageEvent();
    
        bool isSetup;
        bool variablesSet;
};

extern ofEvent<int> stageNotify;

#endif /* defined(__TimelineThread__JTTimeline__) */
