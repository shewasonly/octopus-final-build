//
//  visualScreens.h
//  octoBuild
//
//  Created by Jonathan Thaw on 01/12/2014.
//
//

#ifndef __octoBuild__visualScreens__
#define __octoBuild__visualScreens__

#define FADE_IN 0
#define DISPLAY 1
#define FADE_OUT 2

#define GROWTH 0
#define COMMUNICATION 1
#define ORBIT 2
#define POTENTIAL 3

#include "ofMain.h"
#include "growthSkeleton.h"
#include "globals.h"
#include "subdivision.h"
#include "communicationBody.h"
#include "orbitObject.h"

struct RectBG
{
    float topX;
    float bottomX;
    float width;
};

class VisualScreens
{
    public:
        void setup();
        void update(int pos, int screen, ofColor col);
        void draw(int pos, int screen);
    
        void kinectHandle(int screen);
    
        GrowthSkeleton growth;
    
        Subdivider grid;
    
        CommunicationBody communication;
    
        OrbitObject orbit;
    
        vector<ofPolyline> backgrounds;
};

#endif /* defined(__octoBuild__visualScreens__) */
