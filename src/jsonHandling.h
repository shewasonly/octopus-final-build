//
//  jsonHandling.h
//  octopusSkeleton
//
//  Created by Jonathan Thaw on 13/11/2014.
//
//

#ifndef __octopusSkeleton__jsonHandling__
#define __octopusSkeleton__jsonHandling__

#include "ofMain.h"
#include "ofxJSON.h"
#include "globals.h"

class jsonHandler : public ofThread {
    public :
        void threadedFunction();
        void setup();
        void update(bool full);
        void grabFeeds();
        void saveJSON(string path,ofxJSONElement data);
        
        string getTime();
        
        bool isConnected();
        bool connected;
        
        ofxJSONElement returnFeed(string feed);
        
        ofxJSONElement pingTest;
        
        ofxJSONElement tubes;
        ofxJSONElement weather;
        ofxJSONElement twitter;
        ofxJSONElement headlines;
    
        ofEvent<int> jsonNotify;
        void sendStageEvent();
};

#endif
