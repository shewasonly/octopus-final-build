//
//  clock.cpp
//  TimelineThread
//
//  Created by Jonathan Thaw on 08/12/2014.
//
//

#include "clock.h"

void ClockDisplay::setup()
{
    for(int i = 0; i<200; i++)
    {
        Star curr;
        
        curr.pos.x = ofRandom(0,JTGetWindowWidth());
        curr.pos.y = ofRandom(0,JTGetWindowHeight());
        curr.size = ofRandom(2,7);
        curr.layer = round(ofRandom(1,5));
        curr.vel.x = ofMap(curr.layer,1,5,0.1,0.05);
        curr.vel.y = 0.0;
        
        stars.push_back(curr);
    }
}

void ClockDisplay::update()
{
    for(int i = 0; i<stars.size(); i++)
    {
        stars[i].pos += stars[i].vel;
        
        if(stars[i].pos.x > JTGetWindowWidth()+20)
        {
            stars[i].pos.x = -20.0;
        }
    }
}

void ClockDisplay::draw()
{
    for(int i = 0; i<stars.size(); i++)
    {
        float alpha = 200 - (stars[i].layer * 20);
        ofSetColor(255, 255, 255, alpha);
        ofFill();
        
        ofEllipse(stars[i].pos, stars[i].size, stars[i].size);
    }
    
    
    ofPushMatrix();
    
    ofSetColor(255);
    ofFill();
    GlobalVars::xlargeFont.setAlignment(FTGL_ALIGN_CENTER);
    
    string hour = ofToString(ofGetHours());
    
    if(hour.size()==1)
    {
        hour.insert(0,"0");
    }
    
    string minute = ofToString(ofGetMinutes());
    
    if(minute.size()==1)
    {
        minute.insert(0,"0");
    }
    
    ofTranslate(0, (JTGetWindowHeight()/2) - (GlobalVars::xlargeFont.getLineHeight()*0.75));
    GlobalVars::xlargeFont.formatText(hour+":"+minute,JTGetWindowWidth());
    GlobalVars::xlargeFont.setAlignment(FTGL_ALIGN_LEFT);
    
    ofPopMatrix();
}

void ClockDisplay::resize()
{
    
    for(int i = 0; i<200; i++)
    {
        Star curr;
        
        curr.pos.x = ofRandom(0,JTGetWindowWidth());
        curr.pos.y = ofRandom(0,JTGetWindowHeight());
        curr.size = ofRandom(2,7);
        curr.layer = round(ofRandom(1,5));
        curr.vel.x = ofMap(curr.layer,1,5,0.1,0.05);
        curr.vel.y = 0.0;
        
        stars.push_back(curr);
    }
}