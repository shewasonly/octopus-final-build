//
//  areaTile.h
//  subdivide
//
//  Created by Jonathan Thaw on 01/12/2014.
//
//

#ifndef __subdivide__areaTile__
#define __subdivide__areaTile__

#include <stdio.h>
#include "ofMain.h"
#include "ofxEasingFunc.h"

class AreaRect
{
    public:
        void update();
        void draw(ofColor col);
        
        ofRectangle shape;
        float alpha;
        float setFrame;
        float timeWait;
        float waitFrame;
    
        void fire(float _time);
    
        bool firing;
};

#endif /* defined(__subdivide__areaTile__) */
