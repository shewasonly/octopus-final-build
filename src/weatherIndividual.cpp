//
//  weatherIndividual.cpp
//  octoBuild
//
//  Created by Jonathan Thaw on 27/11/2014.
//
//

#include "weatherIndividual.h"

void WeatherIndividual::setup(float _wid, float _hei, int _index)
{
    width = _wid;
    height = _hei;
    index = _index;
    
    timeFrame = 2;
    
    anim.setup("Rain",width,height);
    
    data = GlobalVars::weatherJSON[index];
}

void WeatherIndividual::update()
{
    
    alpha = ofMap(ofGetElapsedTimef(),setFrame+(index+1),setFrame+(index+1)+timeFrame,0,1,true);
    
    data = GlobalVars::weatherJSON[index];
    anim.setWeather(data["weather"].asString());
    anim.update();
}

void WeatherIndividual::draw()
{
    
    
    ofSetColor(255,255,255,90 - (index*30));
    ofFill();
    ofRect(0,0,width,height);
    
    anim.draw();
    
    ofPushMatrix();
    
    ofTranslate(20,40);
    
    ofSetColor(255,255,255,100);
    ofFill();
    
    GlobalVars::mediumFont.formatText(data["date_day"].asString() + " " + data["date_num"].asString(), width-40);
    
    ofTranslate(-5,130);
    GlobalVars::smallFont.setLineLength(width);
    GlobalVars::smallFont.drawString(data["weather_string"].asString(), 10, 0);
    
    ofPopMatrix();
    
    ofPushMatrix();
    ofTranslate(15,GlobalVars::screens[3].height + 10);
    
    ofTranslate(0,GlobalVars::xsmallFont.getLineHeight());
    GlobalVars::xsmallFont.drawString("High: "+ofToString(data["max_temp"].asInt()) + "°", 10, 0);
    
    ofTranslate(0,GlobalVars::xsmallFont.getLineHeight());
    GlobalVars::xsmallFont.drawString("Low: "+ofToString(data["min_temp"].asInt()) + "°", 10, 0);
    
    ofPopMatrix();
    
    float v = ofxEasingFunc::Quad::easeOut(alpha);
    float boxAlpha = round(ofMap(v,0,1,255,0));
    
    ofColor currColor = GlobalVars::mainColor;
    currColor.a = boxAlpha;
    
    ofSetColor(currColor);
    ofRect(0, 0, width, height);
}

void WeatherIndividual::fadeIn()
{
    alpha = 0;
    setFrame = ofGetElapsedTimef();
}
