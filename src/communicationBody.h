//
//  communicationBody.h
//  Triangulation
//
//  Created by Jonathan Thaw on 05/12/2014.
//
//

#ifndef __Triangulation__communicationBody__
#define __Triangulation__communicationBody__

#include <stdio.h>
#include "ofMain.h"
#include "commNodes.h"
#include "ofxDelaunay.h"

class CommunicationBody {
public:
    void setup(int max);
    void update();
    void draw();
    
    void addPoint(ofPoint pos, float strength);
    
    vector<CommunicationNode> nodes;
    ofxDelaunay triangulation;
    ofPoint lastPoint;
    
    int recycle;
    int maxAmt;
};

#endif /* defined(__Triangulation__communicationBody__) */
