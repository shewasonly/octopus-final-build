//
//  growthBar.cpp
//  growthNext
//
//  Created by Jonathan Thaw on 04/12/2014.
//
//

#include "growthBar.h"

void GrowthBar::setup(float wid, float hei, ofImage &img)
{
    heightPerc = 0;
    targetHeightPerc = 0;
    
    rand = ofRandom(0,1);
    
    image = img;
}

void GrowthBar::update()
{
    float noise = ofNoise(ofGetElapsedTimef()*rand);
    alter = ofMap(noise,0,1,0,100);
    
    if(targetHeightPerc!=heightPerc)
    {
        heightPerc = ofMap(20,0,100,heightPerc,targetHeightPerc);
    }
    
    
    tempHei = ofMap(heightPerc,0,100,0,JTGetWindowHeight());
    tempHei += alter;
}

void GrowthBar::draw(float wid)
{
    ofSetColor(255);
    ofFill();
    if(image.bAllocated())
    {
        image.draw(0, JTGetWindowHeight() - tempHei, wid, tempHei);
    }
}

void GrowthBar::setHeight(float perc)
{
    targetHeightPerc = perc;
}