//
//  visualScreens.cpp
//  octoBuild
//
//  Created by Jonathan Thaw on 01/12/2014.
//
//

#include "visualScreens.h"

void VisualScreens::setup()
{
    growth.setup(50);
    grid.setup();
    communication.setup(400);
    orbit.setup(ofPoint(ofGetWindowWidth()/2,JTGetWindowHeight()/2), 0.1, 20);
}

void VisualScreens::update(int pos, int screen, ofColor col)
{
    switch(screen)
    {
        case GROWTH:
            kinectHandle(screen);
            growth.update();
            break;
        case COMMUNICATION:
            kinectHandle(screen);
            communication.update();
            break;
        case ORBIT:
            kinectHandle(screen);
            orbit.update();
            break;
        case POTENTIAL:
            kinectHandle(screen);
            grid.update(col);
            break;
    }
}

void VisualScreens::draw(int pos, int screen)
{
    switch(screen)
    {
        case GROWTH:
            growth.draw();
            break;
        case COMMUNICATION:
            communication.draw();
            break;
        case ORBIT:
            orbit.draw();
            break;
        case POTENTIAL:
            grid.draw();
            break;
    }
}

void VisualScreens::kinectHandle(int screen)
{
    switch(screen)
    {
        case GROWTH:
            for(int i = 0; i < GlobalVars::kinectBlobs.size(); i++)
            {
                BlobWrapper curr = GlobalVars::kinectBlobs[i];
                
                if(curr.set)
                {
                    
                    int left = floor(curr.flipNormalizedPos.x - (curr.normalizedBoundingBox.getWidth()/2));
                    int right = floor(left + curr.normalizedBoundingBox.getWidth());
                    
                    for(int x = left; x < right; x++)
                    {
                        int index = floor(ofMap(x,0,JTGetWindowWidth(),0,growth.barPoints.size()));
                        int strength = ofMap(curr.depthPerc,0,100,20,100,true);
                        growth.increase(index, strength);
                    }
                }
            }
            break;
        case COMMUNICATION:
            for(int i = 0; i < GlobalVars::kinectBlobs.size(); i++)
            {
                BlobWrapper curr = GlobalVars::kinectBlobs[i];
                
                if(curr.set)
                {
                    for(int i = round(curr.normalizedBoundingBox.y); i < (curr.normalizedBoundingBox.y + curr.normalizedBoundingBox.height); i = i + 75)
                    {
                        float randX = ofRandom(curr.flipNormalizedPos.x-100,curr.flipNormalizedPos.x+100);
                        communication.addPoint(ofPoint(randX,i), 50);
                    }
                }
            }
            break;
        case ORBIT: {
            BlobWrapper curr = GlobalVars::kinectBlobs[0];
            
            if(curr.set)
            {
                if(orbit.gravitySet==false)
                {
                    orbit.setGravity();
                }
                orbit.location.x = curr.flipNormalizedPos.x;
                orbit.location.y = curr.flipNormalizedPos.y;
            }
            else
            {
                if(orbit.gravitySet)
                {
                    orbit.setGravity();
                }
            }
        }
            break;
        case POTENTIAL:
            for(int i = 0; i < GlobalVars::kinectBlobs.size(); i++)
            {
                BlobWrapper curr = GlobalVars::kinectBlobs[i];
                
                if(curr.set)
                {
                    float hei = ofMap(curr.depthPerc,0,100,JTGetWindowHeight(),0,true);
                    ofRectangle checker;
                    checker.set(curr.flipNormalizedPos.x - 25, hei - 25, 50, 50);
                    grid.paintArea(checker);
                }
            }
            break;
    }
}