//
//  particleObject.cpp
//  orbitPLEASE
//
//  Created by Jonathan Thaw on 04/12/2014.
//
//

#include "particleObject.h"

void ParticleObject::setup(ofPoint _location, float _gravity, float _mass)
{
    position = OUT_OF_ORBIT;
    location = _location;
    gravity = _gravity;
    mass = _mass;
    
    for(int i = 0; i < 6; i++)
    {
        dispLocation[i] = location;
    }
}

void ParticleObject::update(ofPoint centre, float _mass)
{
    ofPoint force = attract(centre,_mass,location,mass);
    
    float rotater = ofMap(distance,0,150,1,10);
    
    if(position==EXITING_CENTRE)
    {
        velocity.rotate(rotater);
    }
    else if(position==IN_ORBIT)
    {
        velocity.rotate(rotater);
    }
    else
    {
        applyForce(force);
    }
    
    velocity += acceleration;
    
    if(gravitySet)
    {
        velocity.x = ofClamp(velocity.x, -15, 15);
        velocity.y = ofClamp(velocity.y, -15, 15);
    }
    else
    {
        velocity.x = ofClamp(velocity.x, -5, 5);
        velocity.y = ofClamp(velocity.y, -5, 5);
    }
    
    location += velocity;
    
    if(location.x<-40||location.x>JTGetWindowWidth()+40)
    {
        velocity.x = 0;
        location.x = ofClamp(location.x, -40, JTGetWindowWidth()+40);
    }
    if(location.y<-40||location.y>JTGetWindowHeight()+40)
    {
        velocity.y = 0;
        location.y = ofClamp(location.y, -40, JTGetWindowHeight()+40);
    }
    
    dispLocation[5] = dispLocation[4];
    dispLocation[4] = dispLocation[3];
    dispLocation[3] = dispLocation[2];
    dispLocation[2] = dispLocation[1];
    dispLocation[1] = dispLocation[0];
    dispLocation[0] = location;
    
    
    acceleration *= 0;
    
    path.clear();
    for(int i = 0; i < 6; i++)
    {
        path.lineTo(dispLocation[i]);
    }
}

void ParticleObject::draw()
{
    path.draw();
}

void ParticleObject::applyForce(ofPoint force)
{
    acceleration += force;
}

ofPoint ParticleObject::attract(ofPoint _location, float _mass,ofPoint _location2, float _mass2)
{
    ofPoint force = _location - _location2;
    distance = force.length();
    
    if(distance<150&&position==0)
    {
        position = CAPTURED_IN_ORBIT;
    }
    
    if(distance<20&&position==1)
    {
        position = EXITING_CENTRE;
    }
    
    if(distance<150&&distance>100&&position==2)
    {
        position = IN_ORBIT;
    }
    
    if(distance>300)
    {
        position = OUT_OF_ORBIT;
    }
    
    float dist = ofClamp(distance,5.0,10.0);
    force.normalize();
    
    float strength = (gravity * _mass * _mass2) / (dist * dist);
    force.scale(strength);
    
    return force;
}