//
//  twitter.cpp
//  octoBuild
//
//  Created by Jonathan Thaw on 28/11/2014.
//
//

#include "twitter.h"

void TwitterScreen::setup()
{
    timeTracker = -1;
    destFinalX = 0;
    wid = JTGetWindowWidth() * 0.75;
    data = GlobalVars::twitterJSON[0];
    timeFrame = 2;
    alpha = 0;
    
    for(int i = 0; i < 3; i++)
    {
        bezier[i].set(JTGetWindowHeight()*0.75, JTGetWindowHeight()*0.75, JTGetWindowHeight()*0.75, JTGetWindowHeight()*0.75);
        bezierVel[i].set(ofRandom(-0.5,0.5), ofRandom(-0.5,0.5), ofRandom(-0.5,0.5), ofRandom(-0.5,0.5));
    }
    
    //title = GlobalVars::xsmallFont.formatTextImage("Latest from Twitter  "+data["datetime"].asString(),wid);
    body = GlobalVars::mediumFont.formatTextImage(data["text"].asString(),wid);
}

void TwitterScreen::update()
{
    if(destX!=destFinalX)
    {
        destX += 1;
        
        float timeLapse = ofMap(ofGetElapsedTimef(),setFrame,setFrame+timeFrame,0,1,true);
        float v = ofxEasingFunc::Quad::easeOut(timeLapse);
        destX = round(ofMap(v,0,1,-100,destFinalX));
        alpha = round(ofMap(v,0,1,0,255,true));
        graphicsAlpha = round(ofMap(v,0,1,0,10,true));
        
        if(destX>=destFinalX)
        {
            destX = destFinalX;
        }
    }
    
    ofxJSONElement temp = GlobalVars::twitterJSON[0];
    
    if(data["text"].asString()!=temp["text"].asString())
    {
        data = temp;
        //title = GlobalVars::xsmallFont.formatTextImage("Latest from Twitter  "+data["datetime"].asString(),wid);
        body = GlobalVars::mediumFont.formatTextImage(data["text"].asString(),wid);
    }
    
    
    for(int i = 0; i < 3; i++)
    {
        if(!ofInRange(bezier[i].x, JTGetWindowHeight()*0.5, JTGetWindowHeight()*0.75))
        {
            bezierVel[i].x *= -1;
        }
        if(!ofInRange(bezier[i].y, JTGetWindowHeight()*0.5, JTGetWindowHeight()*0.75))
        {
            bezierVel[i].y *= -1;
        }
        if(!ofInRange(bezier[i].z, JTGetWindowHeight()*0.5, JTGetWindowHeight()*0.75))
        {
            bezierVel[i].z *= -1;
        }
        if(!ofInRange(bezier[i].w, JTGetWindowHeight()*0.5, JTGetWindowHeight()*0.75))
        {
            bezierVel[i].w *= -1;
        }
        
        bezier[i] += bezierVel[i];
    }
}

void TwitterScreen::draw(int time)
{
    if(time!=timeTracker)
    {
        timeTracker++;
        fadeIn();
    }
    if(GlobalVars::isSet)
    {
        for(int i = 0; i < 3; i++)
        {
            bg[i].clear();
            bg[i].setFillColor(ofColor(255,255,255,graphicsAlpha));
            bg[i].moveTo(0,JTGetWindowHeight());
            bg[i].lineTo(0,bezier[i].x);
            bg[i].bezierTo(JTGetWindowWidth()*0.25, bezier[i].y, JTGetWindowWidth()*0.75, bezier[i].z, JTGetWindowWidth(), bezier[i].w);
            bg[i].lineTo(JTGetWindowWidth(),JTGetWindowHeight());
            bg[i].close();
            
            ofFill();
            bg[i].draw(0,50 + (30*i));
        }
        
        ofPushMatrix();
        ofTranslate(40, 120);
        GlobalVars::xsmallFont.formatText("Latest from Twitter  @OctopusVideoWall",wid);
        ofTranslate(0, 15);
        ofSetColor(255,255,255,alpha);
        body.draw(destX, 0);
        ofPopMatrix();
    }
}

void TwitterScreen::fadeIn()
{
    wid = JTGetWindowWidth() * 0.75;
    destX = -100;
    setFrame = ofGetElapsedTimef();
}

