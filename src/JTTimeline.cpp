//
//  JTTimeline.cpp
//  TimelineThread
//
//  Created by Jonathan Thaw on 26/11/2014.
//
//

#include "JTTimeline.h"

void JTTimeline::setup(bool _debug)
{
    debugMode = _debug;
    isSetup = false;
    
    totalTime = 24*60*60;
    baseTime = (ofGetHours()*60*60) + (ofGetMinutes()*60) + (ofGetSeconds());
    //baseTime = (9*60*60*1000) + (30*60*1000) + (0*1000);
    dayCount = 0;
    currentTime = ofGetElapsedTimef() + baseTime;
    
    isSetup = true;
    
    setVariables();
}

void JTTimeline::threadedFunction() {
    
    while(isThreadRunning()) {
        update();
    }
}

void JTTimeline::update()
{
    currentTime = (ofGetElapsedTimef() + baseTime) - (totalTime*dayCount);
    //currentTime = ((ofGetElapsedTimeMillis()*1000) + baseTime) - (totalTime*dayCount);
    
    if(currentTime>totalTime)
    {
        dayCount++;
    }
    
    currHour = (float) (((currentTime)/60)/60);
    currMin = (float) ((currentTime)/60) - (currHour * 60);
    currSecond = (float) ((currentTime) - (currMin * 60)) - (currHour * 60 * 60);
    
    
    if(currentTime>1&&variablesSet==false)
    {
        setVariables();
    }
    else
    {
        if(variablesSet)
        {
            updateVariables();
        }
    }
}

void JTTimeline::draw()
{
    if(debugMode)
    {
        debugStr();
    }
}

void JTTimeline::setVariables()
{
    coloursAmt = 9;
    
    
    colourTimes[0] = timeToMillis(0,00,1);
    mainColours[0] = ofColor(57,58,80);
    
    colourTimes[1] = timeToMillis(9,00,0);
    mainColours[1] = ofColor(240,142,3);
    
    colourTimes[2] = timeToMillis(12,0,0);
    mainColours[2] = ofColor(170,24,80);
    
    colourTimes[3] = timeToMillis(14,0,0);
    mainColours[3] = ofColor(152,88,158);
    
    colourTimes[4] = timeToMillis(19,0,0);
    mainColours[4] = ofColor(2,131,198);
    
    colourTimes[5] = timeToMillis(22,30,0);
    mainColours[5] = ofColor(55,57,81);
    
    colourTimes[6] = timeToMillis(23,59,59);
    mainColours[6] = ofColor(57,58,80);
    
    
    currColour = 0;
    nextColour = 1;
    
    lastTime = currentTime;
    stagePos = 0;
    
    stagesAmt = 4;
    
    stageTimes[0][0] = timeToMillis(0,0,3);
    stageTimes[0][1] = timeToMillis(0,4,54);
    stageTimes[0][2] = timeToMillis(0,0,3);
    stageTimes[0][3] = timeToMillis(0,0,44);
    
    stageTimes[1][0] = timeToMillis(0,0,3);
    stageTimes[1][1] = timeToMillis(0,4,54);
    stageTimes[1][2] = timeToMillis(0,0,3);
    stageTimes[1][3] = timeToMillis(0,1,04);
    
    stageTimes[2][0] = timeToMillis(0,0,3);
    stageTimes[2][1] = timeToMillis(0,4,54);
    stageTimes[2][2] = timeToMillis(0,0,3);
    stageTimes[2][3] = timeToMillis(0,1,44);
    
    stageTick = 0;
    stageTickPos[0] = 0;
    stageTickPos[1] = 0;
    stageTickPos[2] = 1;
    
    currStage = 0;
    oldStage = 0;
    
    
    variablesSet = true;
}

float JTTimeline::timeToMillis(int _hours, int _minutes, int _seconds)
{
    return (_hours*60*60) + (_minutes*60) + (_seconds);
}

void JTTimeline::updateVariables()
{
    for(int c = 0; c < coloursAmt; c ++)
    {
        if(currentTime>colourTimes[c])
        {
            currColour = c;
            nextColour = currColour + 1;
            
            if(nextColour >= coloursAmt)
            {
                nextColour = 0;
            }
        }
        else
        {
            if(c==0)
            {
                currColour = coloursAmt - 1;
                nextColour = 0;
            }
            break;
        }
    }
    
    float diff = ofMap(currentTime,colourTimes[currColour],colourTimes[nextColour],0,1);
    
    lock();
    mainColour = mainColours[currColour].getLerped(mainColours[nextColour],diff);
    unlock();
    
    if(currentTime>=21600&&currentTime<=81000)
    {
        int currStageTick = stageTickPos[stageTick];
        
        if(currentTime>=59400&&currentTime<=68400) // AFTER 4.30pm & BEFORE 7pm
        {
            if(currStageTick==1)
            {
                currStageTick = 2;
            }
        }
        
        if(lastTime+stageTimes[currStageTick][stagePos]<currentTime)
        {
            stagePos++;
            lastTime = currentTime;
            
            if(stagePos>3)
            {
                stagePos = 0;
                
                stageTick++;
                
                if(stageTick>2)
                {
                    stageTick = 0;
                }
                cout<<stageTick<<endl;
            }
            
            currStage = stagePos;
        }
    }
    else
    {
        lastTime = 0;
        currStage = 4;
    }
    
    if(currStage!=oldStage)
    {
        oldStage = currStage;
        sendStageEvent();
    }
}

void JTTimeline::debugStr()
{
    if(isSetup)
    {
        
        string debug = "";
        debug += "Total Time: "+ofToString(totalTime) + "\n";
        debug += "Current Time: "+ofToString(currentTime) + "\n";
        debug += "Normalised current: " + readable() + "\n";
        debug += "Hour: " + ofToString(currHour) + "\n";
        debug += "Minute: " + ofToString(currMin) + "\n";
        debug += "Second: " + ofToString(currSecond) + "\n";
        debug += "Current Colour: " + ofToString(currColour) + "\n";
        debug += "Days: " + ofToString(dayCount) + "\n";
        debug += "Curent stage Pos: " + ofToString(currStage) + "\n";
        
        ofSetColor(0,0,0,100);
        ofFill();
        ofRect(0,0,230,150);
        
        ofSetColor(255);
        ofFill();
        ofDrawBitmapString(debug, 4,12);
    }
}

string JTTimeline::readable()
{
    if(currentTime>1000)
    {
        float num = currentTime;
        
        return ofToString(num);
    }
    else
    {
        return "0";
    }
}

ofColor JTTimeline::returnColor(string selector)
{
    if(selector=="main")
    {
        return mainColour;
    }
}

void JTTimeline::sendStageEvent()
{
    lock();
    ofNotifyEvent(stageNotify, currStage, this);
    unlock();
}









