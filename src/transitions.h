//
//  transitions.h
//  TimelineThread
//
//  Created by Jonathan Thaw on 05/12/2014.
//
//

#ifndef __TimelineThread__transitions__
#define __TimelineThread__transitions__

#include <stdio.h>
#include "ofMain.h"
#include "transitions.h"

class Transitioner {
    public:
        void setup();
        float update();
        void draw();
        
        void play();
    
        ofVideoPlayer transitionVid;
        int playing;
        unsigned long elapsed;
};

#endif /* defined(__TimelineThread__transitions__) */
