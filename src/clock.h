//
//  clock.h
//  TimelineThread
//
//  Created by Jonathan Thaw on 08/12/2014.
//
//

#ifndef __TimelineThread__clock__
#define __TimelineThread__clock__

#include <stdio.h>
#include "globals.h"
#include "ofMain.h"

struct Star
{
    ofPoint pos;
    float size;
    int layer;
    ofPoint vel;
};

class ClockDisplay
{
    public:
        void setup();
        void update();
        void draw();
    void resize();
    
        vector<Star> stars;
};

#endif /* defined(__TimelineThread__clock__) */
