//
//  weatherScreens.h
//  octoBuild
//
//  Created by Jonathan Thaw on 27/11/2014.
//
//

#ifndef __octoBuild__weatherScreens__
#define __octoBuild__weatherScreens__

#include <stdio.h>
#include "ofMain.h"
#include "globals.h"
#include "weatherIndividual.h"

class WeatherScreens
{
    public:
        void setup();
        void update();
        void draw(int time);
        
        WeatherIndividual weather[3];
        int timeTracker;
};

#endif /* defined(__octoBuild__weatherScreens__) */
