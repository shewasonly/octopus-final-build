//
//  headlines.cpp
//  octoBuild
//
//  Created by Jonathan Thaw on 03/12/2014.
//
//

#include "headlines.h"

void HeadlineReader::setup()
{
    timeTracker = -1;
    destFinalX = 0;
    wid = JTGetWindowWidth() * 0.75;
    timeFrame = 2;
    alpha = 0;
    
    switchTime = 12.5;
    switchPos = 0;
    
    //title = GlobalVars::xsmallFont.formatTextImage("Latest from Twitter  "+data["datetime"].asString(),wid);
    body = GlobalVars::mediumFont.formatTextImage(data["content"].asString(),wid);
    
    sucker.loadImage("sucker.png");
    data = GlobalVars::headlinesJSON[switchPos];
}

void HeadlineReader::update()
{
    if(switchStart+switchTime<ofGetElapsedTimef())
    {
        switchPos++;
        
        if(switchPos>=4)
        {
            switchPos = 0;
        }
        
        switchStart = ofGetElapsedTimef();
        destX = -100;
        setFrame = ofGetElapsedTimef();
    }
    
    if(destX!=destFinalX)
    {
        destX += 1;
        
        float timeLapse = ofMap(ofGetElapsedTimef(),setFrame+1,setFrame+1+timeFrame,0,1,true);
        float v = ofxEasingFunc::Quad::easeOut(timeLapse);
        destX = round(ofMap(v,0,1,-100,destFinalX));
        alpha = round(ofMap(v,0,1,0,255));
        
        if(destX>=destFinalX)
        {
            destX = destFinalX;
        }
    }
    
    ofxJSONElement temp = holder[switchPos];
    
    if(data["content"].asString()!=temp["content"].asString())
    {
        data = temp;
        //title = GlobalVars::xsmallFont.formatTextImage("Latest from Twitter  "+data["datetime"].asString(),wid);
        body = GlobalVars::mediumFont.formatTextImage(data["content"].asString(),wid);
    }
}

void HeadlineReader::draw(int time)
{
    if(time!=timeTracker)
    {
        timeTracker++;
        fadeIn();
    }
    if(GlobalVars::isSet)
    {
        float newHei = JTGetWindowHeight()*1.5;
        float newWid = (newHei*sucker.width) / sucker.height;
        
        ofSetColor(255,255,255,10);
        sucker.draw(JTGetWindowWidth()*0.6, -50,newWid,newHei);
        
        
        ofSetColor(255,255,255);
        ofPushMatrix();
        ofTranslate(40, 60);
        GlobalVars::xsmallFont.formatText(data["title"].asString(),wid);
        ofTranslate(0, 15);
        ofSetColor(255,255,255,alpha);
        body.draw(destX, 0);
        ofPopMatrix();
    }
}

void HeadlineReader::fadeIn()
{
    holder[0] =GlobalVars::headlinesJSON[0];
    holder[1] =GlobalVars::headlinesJSON[1];
    holder[2] =GlobalVars::headlinesJSON[2];
    holder[3] =GlobalVars::headlinesJSON[3];
    destX = -100;
    setFrame = ofGetElapsedTimef();
    switchStart = ofGetElapsedTimef();
    switchPos = 0;
    wid = JTGetWindowWidth() * 0.75;
}

