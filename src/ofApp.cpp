#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetFrameRate(60);
    ofSetVerticalSync(false);
    
    debugMode = false;
    
    // Globals -=-=-=-=-=-=-=-=-=-=-
        GlobalVars::setup();
    
    // Timeline -=-=-=-=-=-=-=-=-=-=-=-
        timeline.startThread(true, false);
        timeline.setup(debugMode);
        ofAddListener(timeline.stageNotify, this, &ofApp::switchStage);
        loopNum = 0;
    
    // Random screens -=-=-=-=-=-=-=-=-=-=-
        for(int i = 0; i < 4; i++)
        {
            screensArr[i] = i;
        }
        screensPos = 2;
        stageVal = 0;
        switch(screensArr[screensPos])
        {
            case 0:
                GlobalVars::screenName = "Growth";
                break;
            case 1:
                GlobalVars::screenName = "Communication";
                break;
            case 2:
                GlobalVars::screenName = "Orbit";
                break;
            case 3:
                GlobalVars::screenName = "Potential";
                break;
        }
    
    // Kinect -=-=-=-=-=-=-=-=-=-=-
        kinectCam.start(debugMode);
    
    
    // JSON -=-=-=-=-=-=-=-=-=-=-=-
        json.startThread(true, false);
        json.setup();
        ofAddListener(json.jsonNotify, this, &ofApp::updateJSON);
    
    // Info screens -=-=-=-=-=-=-=-=-=-=-
        info.setup();
    
    // Visual screens -=-=-=-=-=-=-=-=-=-=-
        visual.setup();
    
    // Transition screens -=-=-=-=-=-=-=-=-=-=-
        transition.setup();
    
    // Clock screens -=-=-=-=-=-=-=-=-=-=-
        clock.setup();
}

//--------------------------------------------------------------
void ofApp::update(){
    kinectCam.update();
    
    // Updating stages -=-=-=-=-=-=-=-=-=-=-
        switch(stageVal)
        {
            case 0:
                percTrans = transition.update();
                
                if(percTrans<=0.5)
                {
                    info.update();
                }
                else
                {
                    visual.update(stageVal,screensArr[screensPos],timeline.returnColor("main"));
                }
                transition.update();
                break;
            case 1:
                visual.update(stageVal,screensArr[screensPos],timeline.returnColor("main"));
                info.update();
                break;
            case 2:
                percTrans = transition.update();
                
                if(percTrans<=0.5)
                {
                    visual.update(stageVal,screensArr[screensPos],timeline.returnColor("main"));
                }
                else
                {
                    info.update();
                }
                transition.update();
                break;
            case 3:
                info.update();
                break;
            case 4:
                clock.update();
                break;
        }
}

//--------------------------------------------------------------
void ofApp::draw(){
    GlobalVars::mainColor = timeline.returnColor("main");
    ofBackground(GlobalVars::mainColor);
    
    // Firing stages -=-=-=-=-=-=-=-=-=-=-
        switch(stageVal)
        {
            case 0:
                if(percTrans<=0.5)
                {
                    info.draw(loopNum);
                }
                else
                {
                    visual.draw(stageVal,screensArr[screensPos]);
                }
                transition.draw();
                break;
            case 1:
                //visual.draw(stageVal,screensArr[screensPos]);
                info.draw(loopNum);
                break;
            case 2:
                if(percTrans<=0.5)
                {
                    visual.draw(stageVal,screensArr[screensPos]);
                }
                else
                {
                    info.draw(loopNum);
                }
                transition.draw();
                break;
            case 3:
                info.draw(loopNum);
                break;
            case 4:
                clock.draw();
                break;
        }
     
    GlobalVars::draw(debugMode);
    
    // Wrapper debug -=-=-=-=-=-=-=-=-=-=-
        ofPushMatrix();
            ofTranslate(0,0);
            GlobalVars::debugGlobals(debugMode);
            ofTranslate(200,0);
            timeline.draw();
            ofTranslate(230, 0);
            kinectCam.draw(430,0);
        ofPopMatrix();
}

//===================================================== TIMELINE: Receiving stage switch value
void ofApp::switchStage(int &val) {
    stageVal = val;
    
    if(stageVal==0)
    {
        kinectCam.bgSnap();
        transition.play();
    }
    
    if(stageVal==1)
    {
        loopNum++;
    }
    
    if(stageVal==2)
    {
        info.begin();
        transition.play();
    }
    
    if(stageVal==3)
    {
        
        screensPos++;
        
        if(screensPos>=4)
        {
            screensPos = 0;
            randomScreens();
            ofFile file("alive/"+ofToString(ofGetFrameNum())+".txt",ofFile::WriteOnly);
            file << "saving some data";
            file.close();
        }
        
        switch(screensArr[screensPos])
        {
            case 0:
                GlobalVars::screenName = "Growth";
                break;
            case 1:
                GlobalVars::screenName = "Communication";
                break;
            case 2:
                GlobalVars::screenName = "Orbit";
                break;
            case 3:
                GlobalVars::screenName = "Potential";
                break;
        }
    }

}

//===================================================== TIMELINE: Reorder the screens
void ofApp::randomScreens() {
    random_shuffle(&screensArr[0], &screensArr[4]);
}

//===================================================== JSON: Push the update json to the global headers
void ofApp::updateJSON(int &val) {
    GlobalVars::tubeJSON = json.returnFeed("tube");
    GlobalVars::weatherJSON = json.returnFeed("weather");
    GlobalVars::twitterJSON = json.returnFeed("twitter");
    GlobalVars::headlinesJSON = json.returnFeed("headlines");
}

//===================================================== EXIT FUNCTION
void ofApp::exit() {
    kinectCam.end();
    json.stopThread();
    timeline.stopThread();
}






































//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    if(key==' ')
    {
        kinectCam.bgSnap();
        
        if(debugMode)
        {
            debugMode = false;
            timeline.debugMode = false;
            kinectCam.setDebug(false);
        }
        else
        {
            debugMode = true;
            timeline.debugMode = true;
            kinectCam.setDebug(true);
        }
    }
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    GlobalVars::resize();
    clock.resize();
    info.resize();
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
