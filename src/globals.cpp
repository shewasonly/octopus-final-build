//
//  globals.cpp
//  octoBuild
//
//  Created by Jonathan Thaw on 27/11/2014.
//
//

#include "globals.h"

vector<BlobWrapper> GlobalVars::kinectBlobs;
string GlobalVars::screenName;

ofxFTGLSimpleLayout GlobalVars::xsmallFont;
ofxFTGLSimpleLayout GlobalVars::smallFont;
ofxFTGLSimpleLayout GlobalVars::mediumFont;
ofxFTGLSimpleLayout GlobalVars::largeFont;
ofxFTGLSimpleLayout GlobalVars::xlargeFont;

ofxJSONElement GlobalVars::settingsElem;

ofxJSONElement GlobalVars::tubeJSON;
ofxJSONElement GlobalVars::weatherJSON;
ofxJSONElement GlobalVars::twitterJSON;
ofxJSONElement GlobalVars::headlinesJSON;

ofColor GlobalVars::mainColor;

ofRectangle GlobalVars::screens[6];

bool GlobalVars::isSet;

void GlobalVars::setup()
{
    isSet = false;
    
    string settingsURL = "mysettings.json";
    bool settingsConnect = settingsElem.open(settingsURL);
    
    if(settingsConnect)
    {
        settingsElem.getRawString(true);
        
        xsmallFont.loadFont("apexlight.ttf", settingsElem["xsmall"].asInt());
        smallFont.loadFont("apexlight.ttf", settingsElem["small"].asInt());
        mediumFont.loadFont("apexlight.ttf", settingsElem["medium"].asInt());
        largeFont.loadFont("apexlight.ttf", settingsElem["large"].asInt());
        xlargeFont.loadFont("apexlight.ttf", settingsElem["xlarge"].asInt());
    }
    else
    {
        
    }
    
    resize();
    
    isSet = true;
}

void GlobalVars::draw(bool _debug)
{
    if(_debug)
    {
        for(int i = 0; i < 6; i++)
        {
            ofSetColor(0);
            ofNoFill();
            ofRect(screens[i]);
        }
    }
}

void GlobalVars::debugGlobals(bool _debug)
{
    if(_debug)
    {
        string debug = "";
        
        int set = 0;
        for(int i = 0; i < kinectBlobs.size(); i++)
        {
            if(kinectBlobs[i].set)
            {
                set++;
            }
        }
        
        debug += "Frame rate: " + ofToString(ofGetFrameRate()) + "\n";
        debug += "Num of active blobs: " + ofToString(set) + "\n";
        debug += "Name of screen: \n";
        debug += screenName + "\n";
        
        ofSetColor(0,0,0,100);
        ofFill();
        ofRect(0,0,200,150);
        
        ofSetColor(0);
        ofFill();
        ofDrawBitmapString("Globals debug", 4,14);
        
        ofSetColor(255);
        ofFill();
        ofDrawBitmapString(debug, 4,32);
    }
}

void GlobalVars::resize()
{
    int col = 0;
    int row = 0;
    
    int _wid = JTGetWindowWidth()/3;
    int _hei = JTGetWindowHeight()/2;
    
    for(int i = 0; i < 6; i++)
    {
        screens[i].set(col*_wid, row*_hei, _wid, _hei);
        col++;
        
        if(col>2)
        {
            row++;
            col = 0;
        }
    }
}
