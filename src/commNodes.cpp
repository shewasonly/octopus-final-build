//
//  commNodes.cpp
//  Triangulation
//
//  Created by Jonathan Thaw on 05/12/2014.
//
//

#include "commNodes.h"

CommunicationNode::CommunicationNode(ofPoint last, float dist)
{
    x = ofClamp(ofRandom(last.x - dist, last.x + dist),0,JTGetWindowWidth());
    y = ofClamp(ofRandom(last.y - dist, last.y + dist),0,JTGetWindowHeight());
    vel.set(ofRandom(0.06, 0.08), 0);
    vel.rotate(ofRandom(0, 360));
}

void CommunicationNode::update()
{
    *this += vel;
    
    if(x<0) {
        x = 0;
        vel.x *= -1;
    }
    
    if(y<0) {
        y = 0;
        vel.y *= -1;
    }
    
    if(x>JTGetWindowWidth()) {
        x = JTGetWindowWidth();
        vel.x *= -1;
    }
    
    if(y>JTGetWindowHeight()) {
        y = JTGetWindowHeight();
        vel.y *= -1;
    }
}

void CommunicationNode::draw()
{
    //ofCircle(*this, 2);
}