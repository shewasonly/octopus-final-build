//
//  kinectWrapper.cpp
//  kinectWrapper
//
//  Created by Jonathan Thaw on 25/11/2014.
//
//

#include "kinectWrapper.h"

void KinectWrap::start(bool _debug)
{
    debugMode = _debug;
    drawIt = false;
    gui = new ofxUICanvas();
    
    gui->setDimensions((kinect.width/3)*2,150);
    gui->addToggle("DRAWIT", false);
    gui->addSlider("NEARTHRESH", 0, 255, 100);
    gui->addSlider("FARTHRESH", 0, 255, 100);
    gui->addSlider("ANGLE", 0, 30, 20);
    gui->autoSizeToFitWidgets();
    ofAddListener(gui->newGUIEvent, this, &KinectWrap::guiEvent);
    gui->loadSettings("kinect_settings.xml");
    
    if(!debugMode)
    {
        gui->setVisible(false);
    }
    
    kinect.init();
    kinect.open();
    
    maxBlobs = 5;
    
    for(int i = 0; i<maxBlobs; i++)
    {
        myBlobs.push_back(BlobWrapper(ofPoint(0.0,0.0), kinect.width, kinect.height));
    }
    
    grayImage.allocate(kinect.width, kinect.height);
    grayThreshNear.allocate(kinect.width, kinect.height);
    grayThreshFar.allocate(kinect.width, kinect.height);
    
    farDepth = GlobalVars::settingsElem["farDepth"].asInt();
    nearDepth = GlobalVars::settingsElem["nearDepth"].asInt();
}

void KinectWrap::update()
{
    kinect.update();
    
    if(kinect.isFrameNew())
    {
        grayImage.setFromPixels(kinect.getDepthPixels(), kinect.width, kinect.height);
        
        blurred = grayImage;
        
        if (!background.bAllocated)
        {
            background = blurred;
        }
        
        diff = blurred;
        diff -= background; // UNCOMMENT THIS IF YOU WANT TO DIFFERENCE FROM THE BACKGROUND?! IM NOT SO SURE WHATS BEST
        
        grayThreshNear = diff;
        grayThreshFar = diff;
        grayThreshNear.threshold(nearThreshold, true);
        grayThreshFar.threshold(farThreshold);
        
        mask.allocate(grayImage.width, grayImage.height);
        
        cvAnd(grayThreshNear.getCvImage(), grayThreshFar.getCvImage(), mask.getCvImage(), NULL);
        
        straightMask = diff;
        straightMask.threshold(10);
        
        contourFinder.findContours( mask, 2000, 1000000, maxBlobs, false,false);
        
        for (int i=0; i < maxBlobs; i++)
        {
            if(i<contourFinder.nBlobs)
            {
                myBlobs[i].depth = kinect.getDistanceAt(contourFinder.blobs.at(i).centroid);
                myBlobs[i].depthPerc = ofMap(myBlobs[i].depth,farDepth,nearDepth,0,100,true);
                myBlobs[i].setTarget(contourFinder.blobs.at(i).centroid);
                myBlobs[i].boundingBox = contourFinder.blobs.at(i).boundingRect;
                myBlobs[i].setBlob(contourFinder.blobs.at(i));
                myBlobs[i].update();
            }
            else
            {
                myBlobs[i].set = false;
            }
        }
    }
    returnBlobs();
}

void KinectWrap::draw(int x, int y)
{
    if(debugMode)
    {
        gui->setPosition(x, y);
        if(drawIt)
        {
            ofPushMatrix();
            ofTranslate(0,123);
            int modifier = 3;
            float cWid = kinect.width/modifier;
            float cHei = kinect.height/modifier;
            int views = 4;
            int col = 0;
            int row = 0;
            
            for(unsigned i = 0; i<views; i++)
            {
                ofSetColor(255);
                
                if(col*cWid>=cWid*2)
                {
                    row++;
                    col = 0;
                }
                
                switch(i)
                {
                    case 0:
                        kinect.draw(col*cWid, row*cHei, cWid, cHei);
                        ofSetColor(220,0,50);
                        ofFill();
                        ofDrawBitmapString("Kinect feed", col*cWid + 10, row*cHei + 20);
                        break;
                    case 1:
                        if(mask.bAllocated)
                        {
                            ofPushMatrix();
                            
                            ofTranslate(col*cWid, row*cHei);
                            
                            ofSetColor(255);
                            ofFill();
                            
                            mask.draw(0,0, cWid, cHei);
                            contourFinder.draw(0,0, cWid, cHei);
                            
                            for (int i=0; i < maxBlobs; i++)
                            {
                                if(myBlobs[i].set)
                                {
                                    myBlobs[i].draw(modifier);
                                }
                            }
                            ofSetColor(220,0,50);
                            ofFill();
                            ofDrawBitmapString("Mask threshold", 10, 20);
                            
                            ofPopMatrix();
                        }
                        break;
                    case 2:
                        if(mask.bAllocated)
                        {
                            ofPushMatrix();
                            
                            ofTranslate(col*cWid, row*cHei);
                            
                            ofSetColor(255, 255, 255);
                            ofFill();
                            ofRect(0,0,cWid,cHei);
                            
                            for (int i=0; i < maxBlobs; i++)
                            {
                                if(myBlobs[i].set)
                                {
                                    vector<ofPoint> points = myBlobs[i].blob.pts;
                                    
                                    for(int p = 0; p<points.size(); p = p + 15)
                                    {
                                        ofPoint curr = points[p] / modifier;;
                                        ofSetColor(myBlobs[i].col);
                                        ofFill();
                                        ofEllipse(curr, 2, 2);
                                    }
                                }
                            }
                            
                            ofSetColor(220,0,50);
                            ofFill();
                            ofDrawBitmapString("Point Outline", 10, 20);
                            
                            ofPopMatrix();
                        }
                        break;
                    case 3:
                        background.draw(col*cWid, row*cHei, cWid, cHei);
                        ofSetColor(220,0,50);
                        ofFill();
                        ofDrawBitmapString("Background", col*cWid + 10, row*cHei + 20);
                        break;
                        
                }
                col++;
            }
            ofPopMatrix();
        }
    }
}

void KinectWrap::bgSnap()
{
    background = blurred;
}

void KinectWrap::end()
{
    gui->saveSettings("kinect_settings.xml");
    delete gui;
    kinect.close();
}

void KinectWrap::guiEvent(ofxUIEventArgs &e)
{
    if(e.getName() == "DRAWIT")
    {
        ofxUIToggle *toggle = e.getToggle();
        drawIt = toggle->getValue();
    }
    if(e.getName() == "NEARTHRESH")
    {
        ofxUISlider *slide = e.getSlider();
        nearThreshold = slide->getValue();
    }
    if(e.getName() == "FARTHRESH")
    {
        ofxUISlider *slide = e.getSlider();
        farThreshold = slide->getValue();
    }
    if(e.getName() == "ANGLE")
    {
        ofxUISlider *slide = e.getSlider();
        kinect.setCameraTiltAngle(slide->getValue());
    }
}

void KinectWrap::returnBlobs()
{
    GlobalVars::kinectBlobs = myBlobs;
}

void KinectWrap::setDebug(bool mode)
{
    debugMode = mode;
    gui->setVisible(debugMode);
    
}


